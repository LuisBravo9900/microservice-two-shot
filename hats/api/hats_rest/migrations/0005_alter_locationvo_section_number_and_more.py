# Generated by Django 4.0.3 on 2023-04-20 23:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0004_locationvo_import_href_alter_hat_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locationvo',
            name='section_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='shelf_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
